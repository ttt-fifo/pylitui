#!/bin/bash
# Integrate PyLiTUI with a generic terminal editor
#
# Working with editors:
#    - vi, vim and other vim clones
#    - pico, nano
#    - joe
#    - mcedit (midnight commander editor)
#    - possibly with others
#
# What this script does is simply run the editor and pylitui.py in loop until
# pressing 'q' in PyLiTUI interface.
#
# Install: Configure the script (see below), rename / copy it to your path.
#
# Usage: Start this script instead of your favorite editor. After code edit,
#        quit the editor and PyLiTUI will run automatically. Select the error
#        and editor will open on the desired line. Break the loop by
#        pressing 'q' in PyLiTUI.


# configure these -------------------------------------------------------------
EDITOR=vi
PYLITUI=pylitui.py
TMPDIR=/tmp
# end of configurations -------------------------------------------------------

# the line to goto
LINE=1
# the filename should be the last argument
FILENAME=${@:-1}
# the temporary file name (includes current PID)
TMPFILE=${TMPDIR}/pylitui${$}.tmp

# run them in loop until 'q' pressed in pylitui.py interface
while true;
do
	# run the editor first
	$EDITOR +${LINE} $@

	# run pylitui.py and redirect selected lineno to tmp file
	$PYLITUI $FILENAME 2> $TMPFILE

	# get the desired line from the tmp file
	LINE=$(cat $TMPFILE)
	rm $TMPFILE

	# if lineno == 0, then quit to terminal
	if [[ "$LINE" -eq "0" ]]; then
		exit
	fi
done
