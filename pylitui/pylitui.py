#!/bin/env python3
"""
Python Linters Terminal User Interface.

Author: Todor Todorov <ttodorov@null.net>
License: BSD 2-clause "Simplified" License
"""
import json
import curses
import _curses
import sys
import os
from dataclasses import dataclass


#############################
# Configure these variables #==================================================
#############################

# Configuration of pylint------------------------------------------------------

# Enable / disable pylint
PYLINT = True

# Additional options to pylint
# NOTE: --output-format=json should present here in order to work properly
PYLINT_OPTIONS = ['--output-format=json']

# Configure the colors according to pylint error classification
PYLINT_COLOR = {
    'error': 'red',
    'warning': 'yellow',
    'refactor': 'yellow',
    'convention': 'yellow',
    }


# Configuration of pycodestyle-------------------------------------------------

# Enable / disable pycodestyle
PYCODESTYLE = True

# The color all pycodestyle errors will be reported with
PYCODESTYLE_COLOR = 'yellow'


# Configuration of pydocstyle--------------------------------------------------

# Enable / disable pydocstyle
PYDOCSTYLE = True

# The color all pydocstyle errors will be reported with
PYDOCSTYLE_COLOR = 'green'


# Configuration of mypy--------------------------------------------------------

# Enable / disable mypy
MYPY = False

# Additional mypy options
MYPY_OPTIONS = []

# Configure colors according to mypy classification
MYPY_COLOR = {
    'error': 'yellow',
    'note': 'yellow'
    }


# Other configurations---------------------------------------------------------

# Configure default sort order (may include 'lineno' and 'color')
DEFAULT_SORT_ORDER = ['lineno', 'color']

# NOTE: The TUI color scheme can be changed in class TUI, method init_styles()
# (see below)

###############################
# End variables configuration #================================================
###############################


# conditional imports----------------------------------------------------------
if PYLINT:
    try:
        import pylint.epylint
    except ModuleNotFoundError:
        PYLINT = False

if PYCODESTYLE:
    try:
        import pycodestyle
    except ModuleNotFoundError:
        PYCODESTYLE = False

if PYDOCSTYLE:
    try:
        import pydocstyle
    except ModuleNotFoundError:
        PYDOCSTYLE = False

if MYPY:
    try:
        from mypy.main import main as mypy_main_main
    except ModuleNotFoundError:
        MYPY = False

if PYCODESTYLE or MYPY:
    from io import StringIO
# end of conditional imports---------------------------------------------------


# helper functions / classes---------------------------------------------------
def padstr(string, width):
    """Pad a string to a given width."""
    res = string[:width]
    pad = ' ' * (width - len(string))
    return res + pad


@dataclass
class LintError:
    """Create lint error data object."""

    lineno: int
    color: str
    message: str
# end of helper functions / classes--------------------------------------------


# linter checkers functions----------------------------------------------------
def check_pylint(filename):
    """Lint with pylint."""
    if not PYLINT:
        # pylint is disabled
        return []

    # run pylint and get output
    opts = ' '.join([filename] + PYLINT_OPTIONS)
    pylint_stdout, _ = pylint.epylint.py_run(opts, return_std=True)
    stdout = pylint_stdout.getvalue()
    if stdout:
        errors = json.loads(stdout)
    else:
        errors = []

    # convert output to error objects
    result = []
    for error in errors:
        result.append(
            LintError(lineno=int(error['line']),
                      color=PYLINT_COLOR.get(error['type'], 'red'),
                      message=f"[{error['symbol']}] {error['message']}"))

    return result


def check_pycodestyle(filename):
    """Lint with pycodestyle."""
    if not PYCODESTYLE:
        # pycodestyle is disabled
        return []

    # capture stdout
    old_sys_stdout = sys.stdout
    sys_stdout = StringIO()
    sys.stdout = sys_stdout

    # make pycodestyle checks
    # FIXME: how to exclude some EXXX from checks?
    pycodestyle_checker = pycodestyle.Checker(filename)
    pycodestyle_checker.check_all()

    # restore original stdout
    sys.stdout = old_sys_stdout

    # parse errors to error objects
    result = []
    for line in sys_stdout.getvalue().split('\n'):
        line = line.strip()
        if line.startswith(filename):
            line_array = line.split(':')
            result.append(
                LintError(lineno=int(line_array[1]),
                          color=PYCODESTYLE_COLOR,
                          message=line_array[3].strip()))

    return result


def check_pydocstyle(filename):
    """Lint with pydocstyle."""
    if not PYDOCSTYLE:
        # pydocstyle is disabled
        return []

    # FIXME: How to exclude some DXXX from checks?
    result = []
    with open(os.devnull, 'w') as devnull:
        # stderr is suppressed in code below
        old_sys_stderr = sys.stderr
        sys.stderr = devnull

        # collect errors into error objects
        for error in pydocstyle.check([filename]):
            try:
                lineno = error.line
                message = error.message
            except AttributeError:
                continue

            result.append(
                LintError(lineno=lineno,
                          color=PYDOCSTYLE_COLOR,
                          message=message))

        # restore stderr
        sys.stderr = old_sys_stderr

    return result


def check_mypy(filename):
    """Lint with mypy."""
    if not MYPY:
        # mypy is disabled
        return []

    # prevent mypy_main_main to exit
    old_sys_exit = sys.exit
    sys.exit = lambda x: x

    # run and capture stdout
    stdout = StringIO()
    stderr = StringIO()
    mypy_options = [filename] + MYPY_OPTIONS
    mypy_main_main(None, stdout, stderr, mypy_options)

    # parse stdout into error objects
    result = []
    for line in stdout.getvalue().split('\n'):
        if line.startswith(filename):
            line_array = line.split(':', 3)
            result.append(
                LintError(lineno=int(line_array[1]),
                          color=MYPY_COLOR.get(line_array[2].strip(), 'red'),
                          message=f"[mypy_{line_array[2].strip()}]" +
                          f"{line_array[3]}"))

    # restore sys.exit
    sys.exit = old_sys_exit

    return result
# end of linter checker functions----------------------------------------------


# TUI classes------------------------------------------------------------------
class HelpWin:
    """Create help window."""

    # the help string
    HELPSTR = """
 Up/Down Arrows : Navigation
 Enter          : Select error (GOTO selected line number)
 x              : Exit (GOTO line 1)
 q              : Quit (quit linting)
 c              : Sort by color
 n              : Sort by line number

                  Press any key...
"""
    # help string dimensions
    HELPSTR_HEIGHT = 10
    HELPSTR_WIDTH = 59

    def __init__(self, datawin, styles):
        """Initialize help window object."""
        self.datawin = datawin
        self.styles = styles

        # calculate the window position (center of stdscr)
        height, width = self.datawin.getmaxyx()
        y_start = int((height - self.HELPSTR_HEIGHT - 2) / 2)
        x_start = int((width - self.HELPSTR_WIDTH - 2) / 2)

        # create the window
        self.win = self.datawin.subwin(self.HELPSTR_HEIGHT + 2,
                                       self.HELPSTR_WIDTH + 2,
                                       y_start, x_start)

    def draw(self):
        """Draw the help window."""
        # draw the box
        self.win.attron(self.styles['infowin'])
        self.win.box()

        # draw the help lines
        y_current = 1
        for line in self.HELPSTR.split('\n'):
            self.win.move(y_current, 1)
            self.win.addstr(padstr(line, self.HELPSTR_WIDTH))
            y_current += 1
        self.win.attroff(self.styles['infowin'])

        # display on physical screen
        self.win.refresh()

    def wait_any_key(self):
        """Wait for user to press any key."""
        self.win.getch()


class DataWin:
    """Create data window."""

    # the width of the first column
    COL1_WIDTH = 5
    # the weight of the colors (for sorting purposes)
    COLOR_WEIGHT = {'red': 0, 'yellow': 1, 'green': 2}

    def __init__(self, stdscr, styles, errors):
        """Initialize data window object."""
        self.stdscr = stdscr
        self.styles = styles
        self.errors = errors

        # create ncurses window with the desired geometry
        height, width = self.stdscr.getmaxyx()
        self.win = curses.newwin(height - 1, width, 1, 0)
        # and switch the keypad (can accept arrows from the user)
        self.win.keypad(True)

        # which row is currently highlighted
        self.y_highlight = 0
        # we display from this error index on (for pagination)
        self.error_ix_start = 0

    def draw(self):
        """Draw the data window."""
        height, width = self.win.getmaxyx()

        # draw all error rows from the starting index to the end
        y_current = 0
        for error_ix in range(self.error_ix_start, len(self.errors)):
            if y_current == height:
                # no more space on screen
                break

            if y_current == height - 1:
                # this fixes the bug: cannot draw the last char on the
                # ncurses window
                width -= 1

            # get the desired error by index
            error = self.errors[error_ix]

            # arrange the row style
            if y_current == self.y_highlight:
                style = self.styles[f"{error.color}_highlight"]
            else:
                style = self.styles[error.color]

            # draw line number
            # the line number string from int (right alligned)
            lineno = f"{error.lineno: {self.COL1_WIDTH}d}"
            self.win.attron(style)
            self.win.move(y_current, 0)
            self.win.addstr(lineno)

            # draw the message
            self.win.move(y_current, self.COL1_WIDTH + 1)
            # here we escape newlines and pad to desired screen width
            self.win.addstr(padstr(error.message.replace('\n', '\\n'),
                                   width - self.COL1_WIDTH - 1))
            self.win.attroff(style)

            # jumping to the next row
            y_current += 1

        # finish the window / add all attributes
        self.win.clrtobot()
        self.draw_borders()
        self.draw_pagination_arrows()

        # display on physical screen
        self.win.refresh()

    def draw_borders(self):
        """Draw the data window borders."""
        # this draws only one vertical line between column1, column2
        height, _ = self.win.getmaxyx()
        self.win.move(0, self.COL1_WIDTH)
        self.win.attron(self.styles['borders'])
        self.win.vline(curses.ACS_VLINE, height)
        self.win.attroff(self.styles['borders'])

    def draw_pagination_arrows(self):
        """Draw the data pagination arrows."""
        height, _ = self.win.getmaxyx()
        self.win.attron(self.styles['arrows'])

        if self.error_ix_start > 0:
            # we start drawing from other index than zero, this means there are
            # more rows above the visible area ==> drawing up arrow
            self.win.move(0, self.COL1_WIDTH)
            self.win.addch(curses.ACS_UARROW)

        if self.error_ix_start + height < len(self.errors) - 1:
            # there are more rows below the visible area ==> drawing down arrw
            self.win.move(height - 1, self.COL1_WIDTH)
            self.win.addch(curses.ACS_DARROW)

        self.win.attroff(self.styles['arrows'])

    def sort_errors(self, sort_order):
        """Sort the errors according to given sort order."""
        if not sort_order:
            # user does not care of sorting, so leave it like this
            return

        # we need the sort helper to convert the human readable sort list
        # (e.g. ['lineno', 'color'] to sort callback
        def sort_helper(member):
            result = []
            for sort_element in sort_order:
                if sort_element == 'color':
                    result.append(self.COLOR_WEIGHT[member.color])
                elif sort_element == 'lineno':
                    result.append(member.lineno)
            return result

        self.errors.sort(key=sort_helper)

    def event_loop(self):
        """Get user interaction and act accordingly."""
        while True:
            char = self.win.getch()
            if char == curses.KEY_DOWN:
                self.key_down()
            elif char == curses.KEY_UP:
                self.key_up()
            elif char == ord('n'):
                self.key_n()
            elif char == ord('c'):
                self.key_c()
            elif char == ord('?'):
                self.key_question()
            elif char == ord('q'):
                # quit returns line number zero
                return 0
            elif char == ord('x'):
                # exit returns line number 1 (will go back to code at first ln)
                return 1
            elif (char == curses.KEY_ENTER) or (char == ord('\n')):
                # enter returns line number of the selected row
                error = self.errors[self.error_ix_start + self.y_highlight]
                return error.lineno
            self.draw()

    def key_down(self):
        """Process user interaction key down."""
        height, _ = self.win.getmaxyx()
        y_highlight = self.y_highlight + 1

        if self.error_ix_start + y_highlight == len(self.errors):
            # no more rows to highlight, so do nothing
            return

        if y_highlight == height:
            # we are over the last row on screen ==> paginate down
            self.error_ix_start += 1
        else:
            # highlight the next row
            self.y_highlight = y_highlight

    def key_up(self):
        """Process user interaction key up."""
        y_highlight = self.y_highlight - 1

        if y_highlight < 0:
            # we are above the first row on screen
            error_ix_start = self.error_ix_start - 1
            if error_ix_start < 0:
                # no more rows to display above, so do nothing
                return
            # paginate up
            self.error_ix_start = error_ix_start
        else:
            # highlight previous row
            self.y_highlight = y_highlight

    def key_n(self):
        """Process user interaction keypress 'n'."""
        # sort by line number
        self.sort_errors(['lineno', 'color'])
        # reset the displayed rows
        self.y_highlight = 0
        self.error_ix_start = 0

    def key_c(self):
        """Process user interaction keypress 'c'."""
        # sort by color
        self.sort_errors(['color', 'lineno'])
        # reset the displayed rows
        self.y_highlight = 0
        self.error_ix_start = 0

    def key_question(self):
        """Process user interaction keypress '?'."""
        # draw help win and wait for any key from user
        helpwin = HelpWin(self.win, self.styles)
        helpwin.draw()
        helpwin.wait_any_key()


class TUI:
    """Create Terminal User Interface."""

    # the string for waiting window
    WAITSTR = "Linting code. Please wait..."
    # the waiting string width
    WAITSTR_WIDTH = 28
    # the width of all the texts in title (see method draw_title())
    TITLE_TEXT_WIDTH = 68

    def __init__(self, filename):
        """Initialize Text User Interface."""
        self.filename = filename
        # stdscr will come when ncurses are started
        self.stdscr = None
        self.styles = {}
        # these windows will be created later on
        # titlewin is ncurses window object
        self.titlewin = None
        # waitwin is ncurses window object
        self.waitwin = None
        # datawin is a custom object with user interaction capabilities
        self.datawin = None
        # the line number, selected by user on exit
        self.select_lineno = None

    def wrapme(self, stdscr):
        """Display TUI when wrapped by curses.wrapper()."""
        self.stdscr = stdscr
        self.init_styles()
        self.draw_titlewin()

        # show please wait until code is linted through all linters
        # (this is slow)
        self.draw_waitwin()
        errors = check_pylint(self.filename)
        errors += check_pycodestyle(self.filename)
        errors += check_pydocstyle(self.filename)
        errors += check_mypy(self.filename)
        # delete please wait window
        self.waitwin = None

        # show errors on data window
        self.datawin = DataWin(self.stdscr, self.styles, errors)
        self.datawin.sort_errors(DEFAULT_SORT_ORDER)
        self.datawin.draw()

        # interact with user on data window
        # (at the end returns the line number selected by the user)
        self.select_lineno = self.datawin.event_loop()

    def init_styles(self):
        """
        Init all ncurses styles.

        Here the end-user may change the TUI color scheme
        """
        # colored output
        curses.start_color()
        # this permits 'transparent' color: define it with -1
        curses.use_default_colors()
        # no cursor on screen
        curses.curs_set(False)

        # initialize all needed color pairs
        curses.init_pair(10, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(11, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(12, curses.COLOR_BLUE, -1)
        curses.init_pair(13, curses.COLOR_GREEN, curses.COLOR_BLUE)
        curses.init_pair(14, curses.COLOR_CYAN, curses.COLOR_BLUE)
        curses.init_pair(15, curses.COLOR_GREEN, -1)
        curses.init_pair(16, curses.COLOR_YELLOW, -1)
        curses.init_pair(17, curses.COLOR_RED, -1)
        curses.init_pair(18, curses.COLOR_GREEN, curses.COLOR_MAGENTA)
        curses.init_pair(19, curses.COLOR_YELLOW, curses.COLOR_MAGENTA)
        curses.init_pair(20, curses.COLOR_RED, curses.COLOR_MAGENTA)
        curses.init_pair(21, curses.COLOR_CYAN, -1)

        # assign color pairs to human readable dictionary
        # NOTE: here may add A_BOLD, A_ITALIC, etc.
        self.styles['infowin'] = curses.color_pair(10)
        self.styles['title'] = curses.color_pair(11)
        self.styles['borders'] = curses.color_pair(12)
        self.styles['linter_active'] = curses.color_pair(13) | curses.A_BOLD
        self.styles['linter_inactive'] = curses.color_pair(14)
        self.styles['green'] = curses.color_pair(15) | curses.A_BOLD
        self.styles['yellow'] = curses.color_pair(16) | curses.A_BOLD
        self.styles['red'] = curses.color_pair(17) | curses.A_BOLD
        self.styles['green_highlight'] = curses.color_pair(18) | curses.A_BOLD
        self.styles['yellow_highlight'] = curses.color_pair(19) | curses.A_BOLD
        self.styles['red_highlight'] = curses.color_pair(20) | curses.A_BOLD
        self.styles['arrows'] = curses.color_pair(21)

    def draw_titlewin(self):
        """Draw the title window."""
        _, width = self.stdscr.getmaxyx()

        # init the window with desired geometry
        self.titlewin = curses.newwin(1, width, 0, 0)

        # put initial information
        self.titlewin.move(0, 0)
        self.titlewin.addstr(" ?=Help | Active Linters:",
                             self.styles['title'])

        # the linters are highlighted if active and un-highlighted if inactive
        if PYLINT:
            style = self.styles['linter_active']
        else:
            style = self.styles['linter_inactive']
        self.titlewin.addstr(" pylint ", style)
        self.titlewin.addch("|", self.styles['title'])

        if PYCODESTYLE:
            style = self.styles['linter_active']
        else:
            style = self.styles['linter_inactive']
        self.titlewin.addstr(" pycodestyle ", style)
        self.titlewin.addch("|", self.styles['title'])

        if PYDOCSTYLE:
            style = self.styles['linter_active']
        else:
            style = self.styles['linter_inactive']
        self.titlewin.addstr(" pydocstyle ", style)
        self.titlewin.addch("|", self.styles['title'])

        if MYPY:
            style = self.styles['linter_active']
        else:
            style = self.styles['linter_inactive']
        self.titlewin.addstr(" mypy ", style)
        self.titlewin.addch("|", self.styles['title'])

        # pad the screen to width - 1 (see below why - 1)
        self.titlewin.addstr(padstr(' ', width - self.TITLE_TEXT_WIDTH - 1),
                             self.styles['title'])

        # mitigate the bug 'ncurses cannot draw at the last window cell'
        try:
            self.titlewin.addch(' ', self.styles['title'])
        except _curses.error:
            # silent this error because of the bug
            # (this will still draw the ' ')
            pass

        # display on physical screen
        self.titlewin.refresh()

    def draw_waitwin(self):
        """Draw please wait window."""
        height, width = self.stdscr.getmaxyx()

        # calculate geometry (place at the center)
        y_start = int((height - 3) / 2)
        x_start = int((width - self.WAITSTR_WIDTH + 2) / 2)

        # draw the window
        self.waitwin = curses.newwin(3, self.WAITSTR_WIDTH + 2,
                                     y_start, x_start)
        self.waitwin.attron(self.styles['infowin'])
        self.waitwin.box()
        self.waitwin.move(1, 1)
        self.waitwin.addstr(self.WAITSTR)
        self.waitwin.attroff(self.styles['infowin'])

        # put on physical screen
        self.waitwin.refresh()


def main(filename):
    """Perform main computing."""
    # create tui
    tui = TUI(filename)
    # display tui by wrapping it with the curses helper
    curses.wrapper(tui.wrapme)

    # after tui exit, print the desired line number on stderr
    print(f"{tui.select_lineno}", file=sys.stderr, end='')


if __name__ == "__main__":
    try:
        pymodule_filename = sys.argv[1]
    except IndexError:
        print("Usage:")
        print("pylitui.py /path/to/python_module.py")
        sys.exit(1)

    main(pymodule_filename)
