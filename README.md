# Python Linters Terminal User Interface

TUI wrapper to most used Python Linters (pylint, pycodestyle, pydocstyle, mypy).

### Motivation and Features

As I am writing Python code on multiple systems, using diverse editors and IDEs, I need a portable easy to install and manage interface to check (lint) my Python code. The main features of PyLiTUI are:

* Single interface to most used linters
* TUI interface, because TUI can be used on terminal-only systems and also on GUI systems
* Easy integration to all editors / IDEs
* Easy to install / configure (being a single python script)
* Portable to multiple systems (POSIX compatible for now)

### Install

Download the code:

```
$ git clone https://gitlab.com/ttt-fifo/pylitui.git
```

Copy pylitui.py to your favorite location. This may be somewhere in your path, or a convenient directory as ~/.vimrc, etc. Edit pylitui.py and you will see some options to configure near the start of the script. Test the script manually (see below). Integrate the script with wathever editor presents on the system you are currently working (see below).

### Test if pylitui.py is working properly

Start pylitui.py with argument - the file name of some python module:

```
$ pylitui.py /path/to/your/python/module.py
```

You should see a list of errors in the TUI of pylitui.py (you are not a bug-free person, are you?). At the title row you should see which linters are currently active (green).

Select some arbitrary error and push 'Enter'. PyLiTUI should exit and print the line number of the selected error on the stderr. The line number is printed without trailing newline, so be careful not to miss it. In case you really do not have any errors on the TUI screen (so you are a bug-free person after all, huh) then you press 'q' and on stderr you should see line number '0'.

### Prerequisites

* Python >= 3.7
* working ncurses implementation in your Python version
* Install at least one linter in order to use PyLiTUI. Generic linter install:

```
$ pip install pylint
$ pip install pycodestyle
$ pip install pydocstyle
$ pip install mypy
```

### Configuration

Edit pylitui.py and you will see some configuration variables near the script start. They are heavily commented and explained. In general you can configure the linters usage and small number of other options. In case you need to change the color scheme completely, you need to rewrite `TUI.init_styles()` method. The script intent is for usage by Python developers, so do not be shy to change anything you need.

### Integration with the Editor / IDE

The PyLiTUI means of integration are simple, because it aims to integrate with arbitrary editor or IDE:

* Take the filename of python code to be linted as first argument
* On error selected print the error line number on stderr
* On exit ('x' pressed) print line number '1' on stderr
* On quit ('q' pressed) print line number '0' on stderr. As line number '0' is unexistent, this would be a signal to your Editor / IDE to quit.

See directory /integrations for examples how to integrate with some editors.

NOTE: If someone suceed to integrate PyLiTUI with editor not mentioned into /integrations, please send me a note and I will place it there.

### Usage

In case you forget the controls while in TUI, just press '?' for help screen.

### Screenshots

![screenshot](docs/screenshots/scr01.jpg)

For more screenshots see directory /doc/screenshots:

[scr02.jpg](docs/screenshots/scr02.jpg)

[scr03.jpg](docs/screenshots/scr03.jpg)

### Contributions

* Send me any bugs via the ticketing system or email (I am not a bug-free person, am I?)
* Send me any new integrations in order to place them in /integrations
* Have fun with PyLiTUI

### License

BSD 2-clause "Simplified" License

See file [LICENSE](LICENSE) for details

### Author

Todor Todorov <ttodorov@null.net>

[https://gitlab.com/ttt-fifo](https://gitlab.com/ttt-fifo)
